import networkx as nx
import matplotlib.pyplot as plt 

def get_nodes(G):
    return G.number_of_nodes()

def get_edges(G):
    return G.number_of_edges()

def avg_degree(G):
    degree = []
    for i in G.nodes:
        degree.append(G.degree(i))
    return (sum(degree)/len(degree))

def avg_density(G):
    V = get_nodes(G)
    E = get_edges(G)
    if abs(V) > 1:
        return (2 * E)/(V *(V-1))
    return 0

def diameter(G):
    node_list = list(G.nodes)
    costs = []
    for i in range(0, len(node_list) - 1):
        temp = []
        for j in range(i+1, len(node_list)):            
            try:
                path = nx.shortest_path_length(G, node_list[i], node_list[j])
                temp.append(path)
            except nx.NetworkXNoPath:  
                return ('infinite path length because the graph is not connected')
        costs.append(max(temp))
    return max(costs)

def avg_clustering(G):
    totalCluster = 0
    node = len(G.nodes)
    for i in G.nodes:
        neighbours = G.adj[i]
        if len((neighbours)) > 1:
            sGraph = nx.subgraph(G, neighbours)
            Ei = nx.number_of_edges(sGraph)
            Ki = len(neighbours)
            Ci = (2 * Ei) / ( Ki * (Ki- 1))
            totalCluster += Ci
    return totalCluster / node

def plot_distribution(G):
    distribution = []
    degrees = [G.degree(i) for i in G.nodes]
    diff_degree = set(degrees) 
    for degree in diff_degree:
        distribution.append(degrees.count(degree) / len(G.nodes))
    plt.plot(list(diff_degree), distribution, 'ro')
    plt.xlabel('degree [k]')
    plt.ylabel('distribution [P(k)]')
    return plt.show()
