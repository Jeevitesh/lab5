import networkx as nx
from graph_func import *
import matplotlib.pyplot as plt 
import pandas as pd
import time as time

#GRAPH ONE (filename: politician_edges.csv[393KB])
print("\nInformation of 1st graph:")
df1 = pd.read_csv('Data_sets/Task2/politician_edges.csv')
graph1 = nx.from_pandas_edgelist(df1, source='node_1', target='node_2')
print("Nodes: ",get_nodes(graph1))
print("Edges: ",get_edges(graph1))
print("Average Degree: ",avg_degree(graph1))
print("Density: ",avg_density(graph1))
#print("Diameter: ",diameter(graph1))
print("Average Clustering: ",avg_clustering(graph1))
print(plot_distribution(graph1))
'''
#GRAPH TWO(filename)
print("\nInformation of 2nd graph:")
df2 = pd.read_csv('Data_sets/Task2/lastfm_asia_edges.csv')
graph2 = nx.from_pandas_edgelist(df2, source='node_1', target='node_2')
print("Nodes: ",get_nodes(graph2))
print("Edges: ",get_edges(graph2))
print("Average Degree: ",avg_degree(graph2))
print("Density: ",avg_density(graph2))
#print("Diameter: ",diameter(graph2))
print("Average Clustering: ",avg_clustering(graph2))
print(plot_distribution(graph2))

#GRAPH THREE
print("\nInformation of 3rd graph:")
graph3 = nx.read_edgelist("Data_sets/Task2/as19990808.txt", nodetype = int)
print("Nodes: ",get_nodes(graph3))
print("Edges: ",get_edges(graph3))
print("Average Degree: ",avg_degree(graph3))
print("Density: ",avg_density(graph3))
#print("Diameter: ",diameter(graph3))
print("Average Clustering: ",avg_clustering(graph3))
print(plot_distribution(graph3))

#GRAPH FOUR
print("\nInformation of 4th graph:")
graph4 = nx.read_weighted_edgelist('Data_sets/Task2/DD21.edges')
print("Nodes: ",get_nodes(graph4))
print("Edges: ",get_edges(graph4))
print("Average Degree: ",avg_degree(graph4))
print("Density: ",avg_density(graph4))
#print("Diameter: ",diameter(graph4))
print("Average Clustering: ",avg_clustering(graph4))
print(plot_distribution(graph4))

#GRAPH FIVE
print("\nInformation of 5th graph:")
df2 = pd.read_csv('Data_sets/Task2/company_edges.csv')
graph5 = nx.from_pandas_edgelist(df2, source='node_1', target='node_2')
print("Nodes: ",get_nodes(graph5))
print("Edges: ",get_edges(graph5))
print("Average Degree: ",avg_degree(graph5))
print("Density: ",avg_density(graph5))
#print("Diameter: ",diameter(graph5))
print("Average Clustering: ",avg_clustering(graph5))
print(plot_distribution(graph5))

'''