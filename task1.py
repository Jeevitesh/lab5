from graph_func import *
import networkx as nx

G = nx.read_weighted_edgelist('Data_sets/Task1/insecta-ant-colony1-day37.edges')

print("Nodes: ",get_nodes(G))
print("Edges: ",get_edges(G))
print("Average Degree: ",avg_degree(G))
print("Density: ",avg_density(G))
print("Diameter: ",diameter(G))
print("Average Clustering: ",avg_clustering(G))
print(plot_distribution(G))